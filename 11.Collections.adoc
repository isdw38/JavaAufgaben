= Collections
Dominikus Herzberg, Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/11

include::prelude.adoc[]

Sie kennen bislang Arrays, die es erlauben, eine feste Anzahl von Elementen eines Typs gemeinschaftlich zu verwalten. Was, wenn man die Anzahl der Elemente vorher noch nicht kennt? Für diesen Fall gibt es die sogenannten _Collections_.

.Mehr ist besser
****
Wenn Sie noch ein paar Idee für Aufgaben haben, machen Sie uns Vorschläge!
****

== Recherchen zu Collections

`Collection` ist keine Klasse, sondern ein Interface. Es gibt mehrere Klassen, die das Interface implementieren.

* Suchen Sie die Dokumentation zu `Collection` heraus; verschaffen Sie sich einen Überblick über die deklarierten Methoden
* Zwei wichtige Implementierungen sind `ArrayList` und `Stack`; schauen Sie sich auch hier die deklarierten Methoden an.

Eine `Collection` oder eine Implementierungsklasse wie z.B. `ArrayList` muss wissen, von welchem Typ die aufgenommenen Elemente in der Kollektion sind. Aus diesem Grund muss die `Collection` bzw. die `ArrayList` mit einem Typ parametrisiert (man könnte auch sagen: konfiguriert) werden. Recherchieren Sie dazu im Internet und erstellen Sie ein kleines Beispiel in der JShell:

* Ihre Kollektion `c` soll durch eine `ArrayList` realisiert werden
* Fügen Sie der Kollektion zwei `Integer`-Elemente hinzu, wie z.B. `2` und `5`
* Zeigen Sie, wie die Größe der Kollektion anwächst

Durch die Elemente einer Kollektion kann man mit der sogenannten _foreach_-Variante der `for`-Schleife iterieren.

* Recherchieren Sie, wie diese Form der `for`-Schleife anzuwenden ist
* Iterieren Sie über die Elemente der Kollektion `c` und geben Sie die Elemente per `System.out.printf` aus

include::preDetailsSolution.adoc[]
Die Links zu:

* Collection: https://docs.oracle.com/javase/9/docs/api/java/util/Collection.html
* ArrayList: https://docs.oracle.com/javase/9/docs/api/java/util/ArrayList.html
* Stack: https://docs.oracle.com/javase/9/docs/api/java/util/Stack.html

Eine Kollektion aufsetzen und nutzen:
----
jshell> Collection<Integer> c = new ArrayList<>()
c ==> []

jshell> c.size()
$30 ==> 0

jshell> c.add(2)
$31 ==> true

jshell> c.size()
$32 ==> 1

jshell> c.add(5)
$33 ==> true

jshell> c.size()
$34 ==> 2
----

Iteration und Ausgabe der Kollektionswerte
----
jshell> for(Integer i : c) System.out.printf("Element %d\n",i);
Element 2
Element 5
----
include::postDetails.adoc[]

== Warenlager

In dieser Aufgabe soll es um die "Selbstverwaltung" von Dingen gehen. Jede neue Instanz eines Dings (`Thing`) bekommt mit dem Konstruktor eine eindeutige Kennung (_identity_ oder auch kurz ID) als Ganzzahl zugeordnet. Keine Kennung darf doppelt vorkommen, sonst wird eine Ausnahme "illegales Argument" geworfen. Zum schnellen Abgleich mit bereits vergebenen IDs soll eine statische Variable namens `IDs` vom Typ `Set` genutzt werden. Alle neuen Dinge sind außerdem in einer statischen Variable namens  `inventory` vom Typ `Collection` einzutragen.

Folgende Methoden sind zu implementieren:

* `setPrice` ordnet einem Ding einen Preis zu.
* `getPrice` liefert den aktuellen Preis des Dings zurück
* `find` (statisch) liefert zu einer gegebenen Identität die entsprechende Ding-Instanz zurück
* `showInventory` (statisch) gibt alle Dinge anhand ihrer ID und ihrem Preis auf der Konsole aus

Hinweise:

* Sichern Sie Instanzvariablen gegen den Zugriff von außen ab
* Schützen Sie die Kennung (Identität) gegenüber Änderungen
* Nutzen Sie eine Variante der `for`-Schleife, die als _foreach_-Schleife bezeichnet wird

.Lernziele
* Sie verwenden die Interfaces `Collection` und `Set` und machen sich mit den verfügbaren Methoden vertraut
* Sie erlernen, dass Interfaces auch Referenztypen sind
* Sie können eine geeignete Implementierungen zu einer `Collection` wählen, sie parametrisieren und nutzen
* Sie werfen an geeigneten Stellen Ausnahmen, um ungültige Argumente abzufangen
* Sie setzen die sogenannte _foreach_-Schleife ein

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Thing.java[]
----
include::postDetails.adoc[]

== Umwandlung (Array -> Set)

Implementieren sie die folgenden Methoden mit einem Objekt vom Typ `Set` statt mit Arrays. Vergewissern sie sich, dass ihre Implementierung genau so funktioniert, wie die Array-Implementierung.

[TIP]
====
Denken Sie daran, dass ein `Set` per Definition keine Duplikate enthalten kann.
====

[source,java]
----
include::{sourcedir}/transformSet.java[]
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/transformSet_solution.java[]
----
include::postDetails.adoc[]

== Umwandlung (Array -> List)

Implementieren sie die folgenden Methoden mit einem Objekt vom Typ `List` statt mit Arrays. Vergewissern sie sich, dass ihre Implementierung genau so funktioniert, wie die Array-Implementierung.

[source,java]
----
include::{sourcedir}/transformList.java[]
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/transformList_solution.java[]
----
<1> Auch Methoden können Typparameter haben. Wenn man die Methode so definiert wie in dieser Musterlösung, kann man sie für Listen beliebigen Typs verwenden. Es wäre aber auch eine korrekte Lösung, wenn man den Typparameter weglässt und mit `List<Integer>` arbeitet statt mit `List<E>`.
<2> Der sogenannte _Diamantoperator_ `<>` kann bei der Definition von Variablen eines generischen Typs verwendet werden. Der Compiler leitet dann die Typparameter von dem Variablentyp ab.
include::postDetails.adoc[]

== Umwandlung (Array -> Map)

Implementieren sie die folgenden Methoden mit einem Objekt vom Typ `Map` statt mit Arrays. Vergewissern sie sich, dass ihre Implementierung genau so funktioniert, wie die Array-Implementierung.

[source,java]
----
include::{sourcedir}/transformMap.java[]
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/transformMap_solution.java[]
----
include::postDetails.adoc[]

== Suche in Collections

Schreiben Sie eine Methode `find`, die ein Objekt vom Typ `java.util.Collection<Integer>` und ein Objekt vom Typ `Integer` übernimmt und den Index zurückgibt, an dem der gesuchte Integer in der Collection steht. Wird der Integer nicht gefunden, soll der Wert `-1` zurückgegeben werden.

[TIP]
====
Das Interface `Collection` ist zu allgemein um einen Zugriff über einen Index zu erlauben. Stattdessen müssen Sie hier mit Iteratoren (oder einer _foreach_-Schleife) arbeiten.
====

Schaffen Sie es, Ihre Methode so zu erweitern, dass sie für einen beliebigen Typen `T` funktioniert statt nur für `Integer`?

== Binäre Suche

Wenn man schon weiß, dass eine Liste oder ein Array sortiert ist, kann man einen deutlich performanteren Algorithmus zum Suchen eines Elements verwenden, als einfach jedes Element zu überprüfen. Bei dieser sogenannten https://de.wikipedia.org/wiki/Bin%C3%A4re_Suche[_binären Suche_] schaut man sich immer das Element in der Mitte des noch zu durchsuchenden Bereichs an. Entweder ist dieses Element schon das gesuchte Element oder man muss nur noch links davon (wenn das gesuchte Element kleiner ist) oder rechts davon (wenn das gesuchte Element größer ist) weitersuchen.

Implementieren Sie die binäre Suche in `Collection`-Objekten als eigene Methode `binarySearch`.

== Implementierung einer eigenen ArrayList

Grundsätzlich gilt: Immer wenn Sie eine Datenstruktur benötigen oder ein bestimmtes Feature umsetzten wollen recherchieren Sie zuerst, ob es das
nicht schon im JDK oder ggf. auch in anderen Libaries gibt. Denn Codewiederverwendung hilft Fehler zu vermeiden, da man auf Bewährtes und (hoffentlich) getesten Code zurückgreift. Und es spart Zeit, da sich zwar in die API eingearbeitet, aber nichts entwickelt werden muss.

Bei dieser Aufgabe weichen wir nun davon ab: Implementieren Sie Ihre eigene `ArrayList`, die zunächst nur Strings aufnehmen kann. Dies einmal
auszuprobieren hilft Ihnen, ein besseres Verständnis für die Funktionaliät der regulären `ArrayList` zu bekommen.

=== Implementierung ohne Generics

Die Funktionalität Ihrer Klasse `MyArrayList` beschränken Sie  dabei auf folgende Features:

* `boolean add(String e)`
* `String get(int index)`
* `boolean isEmpty()`
* `String remove(int index)`
* `int size()`
* `Object[] toArray()`

Zum Erzeugen nutzen Sie einen Konstruktor `MyArrayList()`.

=== Implementierung mit Generics

Erweitern Sie Ihre Implementierung so, dass Sie beliebige Typen aufnehmen
können, ebenso, wie es die "echte" `ArrayList` kann. Mindestens diese drei Methoden
müssen Sie anpassen in:

* `boolean add(E e)`
* `E get(int index)`
* `E remove(int index)`

Was bedeutet das `E`. Überlegen Sie, was noch überarbeitet werden muss.
