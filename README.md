# Java-Aufgaben

Eine Sammlung von Aufgaben zur Java-Programmierung findet sich in diesem Repo. Die Aufgaben sind entstanden im Rahmen der Veranstaltung "Objektorientierte Programmierung".

Um Programme für Java entwickeln zu können, müssen Sie das _Java Development Kit_ (JDK) auf Ihrem Rechner installieren:

* [Installationsanleitung](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/0.InstallationJDK.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/0.InstallationJDK.adoc))

Für das WS2017 sind zur Bearbeitung und Ansicht fertiggestellt die folgenden Pools an Aufgaben:

## 1. Aufgabenpool

> ACHTUNG: Die Texte nutzen ein relativ neues HTML-Feature, das Tag `<details>`, damit Sie Lösungen wahlweise auf- und zuklappen können. Leider funktioniert das Tag (noch) nicht in Microsofts Internet Explorer. Verwenden Sie deshalb einen anderen, aktuellen Browser.

* [Formale Sprachen und Grammatiken](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/1.FormaleGrammatik.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/1.FormaleGrammatik.adoc))
* [Grundlagen und Arrays](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/2.GrundlagenUndArrays.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/2.GrundlagenUndArrays.adoc))
* [Aufgaben mit Methode](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/3.AufgabenMitMethode.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/3.AufgabenMitMethode.adoc))
* [Zeichenketten](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/4.Zeichenketten.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/4.Zeichenketten.adoc))

## 2. Aufgabenpool

* [Klassen](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/5.Klassen.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/5.Klassen.adoc))
* [Vererbung](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/6.Vererbung.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/6.Vererbung.adoc))
* [Enumerationen](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/7.Enumerationen.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/7.Enumerationen.adoc))
* [Interfaces](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/8.Interfaces.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/8.Interfaces.adoc))
* [Exceptions](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/9.Exceptions.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/9.Exceptions.adoc))

## 3. Aufgabenpool (**neu**)

* [Generics](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/10.Generics.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/10.Generics.adoc))
* [Collections](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/11.Collections.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/11.Collections.adoc))

## Outtakes

Die hier vorgestellten Outtakes sind nicht direkt prüfungsrelevant. Der Code ist schwieriger und anspruchsvoller als das, was Sie in den ersten Wochen zu Java lernen. Gegen Ende des Semesters sollten Sie jedoch weitaus besser mit den Outtakes zurecht kommen.

* [Outtakes](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/Outtakes.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/Outtakes.adoc))

## Allgemeine Hinweise

Die meisten Aufgaben wurden erstellt mit dem _Early Access Release_ für Java 9. Es hat kleine Änderungen zum nun offiziellen Release gegeben, die sich noch an einigen wenigen Stellen verstecken.

* Seinerzeit konnte man `printf` in der JShell verwenden; nun muss es wie gewohnt `System.out.printf` heißen. 

### Logo-Quelle

Das [Logo](https://pixabay.com/de/caf%C3%A9-java-logo-kaffee-151346/) stammt aus Pixabay und ist frei nutzbar.