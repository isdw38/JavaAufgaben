= 8. Woche: OO-Aufgaben
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V0.9, 2016-12-02
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/8
:imagedir: ./images

// Topics: https://git.thm.de/dhzb87/oop/issues/3

== Reguläre Ausdrücke

Reguläre Ausdrücke sind eine kleine Mini-Programmiersprache, mit der man Suchmuster für Zeichenfolgen in Zeichenketten definiert. Eine kurze Einführung in reguläre Ausdrücke liefert z.B. das "offene Buch" des Rheinwerk-Verlags "Java ist auch eine Insel" von Christian Ullenboom:

http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_04_007.htm (Kap. 4.7)

Reguläre Ausdrücken kommen zum Beispiel bei der Verarbeitung von Textdateien oder Nutzereingaben zum Einsatz, und es gibt viele interessante Methoden für reguläre Ausdrücke. Für die folgenden Aufgaben genügt es jedoch, einen einfachen Ansatz zu wählen, denn es kommt einzig darauf an, "gute" reguläre Ausdrücke zu formulieren. Das Beispiel enthält einen regulären Ausdruck, der festlegt, welche Zeichenfolge eine vorzeichenbehaftete Ganzzahl ausmacht.

----
jshell> Pattern.matches("(0|[+-]?([1-9]\\d*))","+123")
$141 ==> true

jshell> Pattern.matches("(0|[+-]?([1-9]\\d*))","007")
$142 ==> false
----

=== Datumsangabe

Formulieren Sie einen regulären Ausdruck, der Datumsangaben der Form `YYYY-MM-DD` (Jahr, Monat und Tag durch einen Strich getrennt) erkennt.

* Ignorieren Sie bei der ersten Lösung die Abhängigkeit der Tagesangabe vom Monat
* Bei der zweiten Lösung berücksichtigen Sie die Abhängigkeit des Tags vom Monat; die mögliche, zusätzliche Abhängigkeit vom Jahr ist durch reguläre Ausdrücke allein nicht sinnvoll realisierbar

Zeigen Sie, dass Sie, dass Ihre Lösung Datumsangaben (unter der gegebenen Einschränkung) richtig erkennt und ungültige Angaben verwirft.

=== Email-Adresse

Eine Email-Adresse setzt sich aus zwei Teilen zusammen: dem Anteil vor dem @-Zeichen, dem lokalen Anteil, und dem Anteil nach dem @-Zeichen, der Domäne; siehe z.B. auch den Wikipedia-Beitrag zur https://de.wikipedia.org/wiki/E-Mail-Adresse[Email-Adresse].

Formulieren Sie einen regulären Ausdruck, der im lokalen Anteil eine beliebige Folge von Zeichen zulässt. Die Domäne besteht mindestens aus einem Domänennamen aus mindestens einem Zeichen, einem Punkt und einem Top-Level-Domänennamen aus minimal zwei Zeichen.

CAUTION: Benutzen Sie Ihren regulären Ausdruck niemals zur Überprüfung von Email-Adressen in einem "echten" Programm. Für die Zwecke der Übung haben wir den Aufbau vereinfacht; die tatsächlichen Regeln sind deutlich komplizierter.

Einen Eindruck, wie kompliziert es sein kann, eine Email-Adresse korrekt zu erkennen, vermitteln Ihnen die Beiträge zu der Frage http://stackoverflow.com/questions/46155/validate-email-address-in-javascript["Validate email address in JavaScript?"] auf http://stackoverflow.com/. Wenn Sie es richtig ernst meinen, sollten Sie zur Validierung (Überprüfung) von Email-Adressen z.B. die https://java.net/projects/javamail[JavaMail-API]
 nutzen.

== Standardbibliothek

=== Bildbearbeitung

Mit der Methode `javax.imageio.ImageIO.read` können Sie Bilddateien in gängigen Formaten (JPEG, PNG, BMP, GIF) laden. Das zurückgegebene Objekt vom Typ `java.awt.image.BufferedImage` erlaubt Ihnen z.B. mit der Methode `getRGB` bzw. `setRGB` auf die Farbwerte einzelner Pixel zuzugreifen. Dabei kann die Kodierung der einzelnen Werte variieren. Die gängigsten Kodierungstypen sind `INT_RGB` und `INT_ARGB`. In diesen Kodierungen repräsentieren die ersten 8 Bits den Blaukanal und die darauffolgenden Achterblöcke jeweils den Grün-, Rot- und (bei `INT_ARGB`) Alphakanal. Kompliziertere Zeichenmethoden erreichen Sie über die Methode `createGraphics`, die ein Objekt vom Typ `java.awt.Graphics2D` zurückgibt, das z.B. die Methode `drawLine` zum Zeichnen einer geraden Linie anbietet. Genauso wie sie geladen werden können Bilder auch mit `javax.imageio.ImageIO.write` wieder auf das Dateisystem geschrieben werden.

Experimentieren Sie ein wenig mit den Möglichkeiten dieser Bibliotheksklassen und lösen sie z.B. die folgenden Aufgaben:

. Erstellen Sie ein leeres `BufferedImage` mit dem Konstruktoraufruf `new BufferedImage(256, 256, BufferedImage.TYPE_INT_RGB)`. Füllen Sie von diesem Bild jedes Pixel mit einer zufälligen Farbe und speichern Sie das Ergebnis als Bilddatei im PNG-Format.
. Laden Sie ein Bild vom Dateisystem und bestimmen Sie die durchschnittliche Farbe aller Pixel.
. Wandeln Sie ein farbiges Bild in Graustufen um, oder extrahieren sie den Rot-, Grün- oder Blaukanal.
. Laden Sie ein Bild in Graustufen oder wandeln sie ein Bild in Graustufen um und führen dann eine https://en.wikipedia.org/wiki/Gamma_correction[Gammakorrektur] durch.
. Einfache Kantenerkennungsalgorithmen basieren auf der Idee, dass man die "Ableitung" eines Bildes errechnen kann, indem man ein neues Bild von der gleichen größe erstellt und darin den Graustufenwert jedes Pixels auf die absolute Differenz der Graustufenwerte dieses und des nächsten Pixels (in x- oder y-Richtung) setzt. Wenn man dieses Verfahren einmal in x-Richtung und dann noch einmal in y-Richtung anwendet, erhält man ein Bild in dem Regionen mit größeren Veränderungen (also z.B. Kanten) heller sind als Regionen mit weniger Änderungen. Schaffen Sie es ein solches Verfahren zu implementieren?
+
_Tipp: Die Differenzen im Bild werden in der Regel nicht sehr groß sein. Wenn Sie sich das Ergebnis wieder als Bilddatei anzeigen lassen wollen ist es also sinnvoll, die Graustufenwerte vorher mit einem Faktor (z.B. Faktor 10 oder 20) zu multiplizieren._




////
Ideen:
- Enums
  -- Einfache arithmetische Ausdrücke als enum
  -- Enum von Tieren mit Geräuschen
  -- Enum von Rollen (Developer, Master, ...)
    --- Erweiterbarkeit
    --- Alternative: static-variablen + int
      ---- Nachteile aufzählen?
- Collections
  -- Welche der folgenden Zuweisungen sind korrekt? (unterschiedliche collection-typen)
  -- Funktionen mit Arrays gegeben => Umwandeln in Collections
  -- Codesonne als Map (mit enums!)
  -- ArrayList nachbauen
  -- Wann ist ArrayList besser, wann LinkedList?
  -- Code mit List vs Code mit ArrayList -> Was ist besser? Warum?
- Standardbibliothek
  -- Swing
  -- BufferedImage
  -- Sound (javax.sound.sampled, javax.sound.midi)
  -- Dates (java.time)
  -- Regex
- Knobelkram
  -- Generisches equals mit Inspection
  -- Project Lombok
////
