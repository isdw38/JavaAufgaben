import java.io.*;
import java.util.*;

class RandomSplit {
    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.out.println("Usage: inFile outFile1 outFile2 [string]");
            System.exit(1);
        }
        String inFileName = args[0];
        String outFile1Name = args[1];
        String outFile2Name = args[2];
        BufferedReader inFile = new BufferedReader(new FileReader(inFileName));
        BufferedWriter[] outFile = {
            new BufferedWriter(new FileWriter(outFile1Name)),
            new BufferedWriter(new FileWriter(outFile2Name))
        };

        ArrayList<String> in = new ArrayList<>();
        String line;
        String attach = args.length >= 4 ? args[3] : ""; 

        while((line = inFile.readLine()) != null)
            if (!line.trim().isEmpty()) in.add(line);
        Collections.shuffle(in);

        for(int i = 0; i < in.size(); i++) {
            int choose = i % 2;
            outFile[choose].write(in.get(i) + attach);
            outFile[choose].newLine();
        }

        inFile.close();
        outFile[0].close();
        outFile[1].close();
        System.exit(0);
    }
}