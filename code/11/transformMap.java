void goShopping() {
  int count = 0;
  String[] article = new String[4];
  double[] value = new double[4];
  article[0] = "Chips";
  value[0] = 1.5;
  count++;
  article[1] = "Fertigpizza";
  value[1] = 2;
  ++count;
  article[2] = "Jede Menge Bier";
  value[2] = 10;
  count++;
  double sum = 0;
  for (int i = 0; i < count; i++)
    sum += value[i];

  for (int i = 0; i < count; i++) {
    printf("%20s: %.2f\n", article[i], value[i]);
  }
  printf("%20s: %.2f","SUMME", sum);
}


String bestStudent(String[] stud,double[] points) {
  double best = points[0];
  int besti = 0;
  for(int i = 0; i < stud.length; i++) {
    if(points[i] > best) {
      best = points[i];
      besti = i;
    }
  }
  return stud[besti];
}
