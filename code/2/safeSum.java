double safeSum(double[] ar) {
    double sum = 0;
    for(int i = 0; i < ar.length; i++) {
        if(Double.isNaN(ar[i])) continue;
        sum += ar[i];
    }
}