int theWinnerIs(int[] ring) {
    if (ring.length == 1) return ring[0];
    int[] r = new int[ring.length-1];
    for(int i = 2; i <= ring.length; i++)
        r[i-2] = ring[i % ring.length];
    return theWinnerIs(r);
}