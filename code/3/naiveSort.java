int indexOfMin(int[] a, int from) {
  int i = from;
  for(int j = from + 1; j < a.length; j++)
    if (a[j] < a[i]) i = j;
  return i;
}

void swap(int[] ar, int i, int k) {
  int tmp = ar[i];
  ar[i] = ar[k];
  ar[k] = tmp;
}