import java.util.function.*;

class UpnCalc {
    public static void main(String[] args) {
        System.out.println(new Stapel());
        System.out.println(new Stapel().push(3));
        System.out.println(new Stapel().push(3).push(4).mul());
    }
}

interface UPNRechner {
    Stapel push(Integer item);
    Stapel drop();
    Integer top();
    Stapel add();
    Stapel sub();
    Boolean isEmpty();
}

class Stapel implements UPNRechner {
    Stapel predecessor;
    Integer item;

    private Stapel(Stapel s, Integer i) {
        predecessor = s;
        item = i;
    }

    Stapel() { this(null,null); }

    public Stapel push(Integer item) {
        return new Stapel(this,item);
    }

    public Integer top() {
        if (predecessor == null) throw new IllegalArgumentException("Stack Underflow");
        return item;
    }

    public Stapel drop() { 
        if (predecessor == null) throw new IllegalArgumentException("Stack Underflow");
        return predecessor;
    }

    public Stapel add() {
        Integer x = drop().top();
        Integer y = top();
        return drop().drop().push(x + y);
    }

    public Stapel sub() {
        return drop().push(-top()).add();
    }

    Stapel op(BiFunction<Integer,Integer,Integer> f) {
        Integer x = drop().top();
        Integer y = top();
        return drop().drop().push(f.apply(x,y));
    }

    Stapel mul() { return op((x,y) -> x * y); }

    public Boolean isEmpty() { return (predecessor == null); } 

    private String show() {
        if (predecessor == null) return "[";
        return predecessor.show() + " " + item;
    }

    public String toString() {
        return show() + " ]";
    }
}

